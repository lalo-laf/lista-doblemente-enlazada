# Lista doblemente enlazada

## Descripción
Implementación en C++.

En una lista enlazada cada nodo apunta únicamente al nodo siguiente de la lista, mientras que en una lista doblemente enlazada cada nodo apunta, además, al nodo anterior. Por otro lado, una lista doblemente enlazada tiene un puntero al primer elemento y un puntero al último elemento. En la siguiente imagen se puede ver un gráfico de la estructura, que representa la secuencia [7,4,2].

![Lista doblemente enlazada](https://gitlab.com/lalo-laf/lista-doblemente-enlazada/-/raw/main/gr%C3%A1fico_lista_doblemente_enlazada.png)

La clase exporta las siguientes funciones:  

- `Lista();` *Constructor por defecto.*  
- `Lista(const Lista& l);` *Constructor por copia.*  
- `~Lista();` *Destructor.*  
- `Lista& operator=(const Lista& aCopiar);` *Operador de asignación.*  
- `void agregarAdelante(const int& elem);` *Agrega un elemento al principio.*  
- `void agregarAtras(const int& elem);` *Agrega un elemento al final.*  
- `void eliminar(Nat i);` *Elimina el i-ésimo elemento.*  
- `int longitud() const;` *Devuelve la cantidad de elementos.*  
- `const int& iesimo(Nat i) const;` *Devuelve una referencia const al elemento en la i-ésima posición.*  
- `int& iesimo(Nat i);` *Devuelve una referencia al elemento en la i-ésima posición.*  
- `void mostrar(ostream& o);` *Muestra la lista en el output stream o.*

## Valgrind
En este proyecto utilicé Valgrind para verificar que la implementación no tiene problemas de memoria, como leaks o fugas, usos de memoria no inciailizada, o lectura/escritura de memoria liberada.

## GTest
En la carpeta [tests](https://gitlab.com/lalo-laf/lista-doblemente-enlazada/-/tree/main/tests) hay un conjunto de pruebas que hacen uso de la biblioteca de Google Test.
