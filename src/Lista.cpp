#include "Lista.h"
#include <cassert>

void Lista::copiarNodos(const Lista& o) {
    Nodo* actual = o._pri;
    while (actual != nullptr) {
        agregarAtras(actual->valor);
        actual = actual->sig;
    }
}

void Lista::destruirNodos() {
    Nodo* actual = _pri;
    while (actual != nullptr) {
        Nodo* siguiente = actual->sig;
        delete actual;
        actual = siguiente;
    }
    _pri = nullptr;
    _ult = nullptr;
}

Lista::Lista() : _pri(nullptr), _ult(nullptr) {
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
    //También puedo inicializar una lista vacía y hacer:
    //copiarNodos(l);
}

Lista::~Lista() {
    destruirNodos();
}

Lista& Lista::operator=(const Lista& aCopiar) {
    destruirNodos();
    copiarNodos(aCopiar);
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nuevo = new Nodo(nullptr, elem, nullptr);
    if (_pri == nullptr) {
        _pri = nuevo;
        _ult = nuevo;
    } else {
        nuevo->sig = _pri;
        _pri->ant = nuevo;
        _pri = nuevo;
    }
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nuevo = new Nodo(nullptr, elem, nullptr);
    if (_pri == nullptr) {
        _pri = nuevo;
        _ult = nuevo;
    } else {
        nuevo->ant = _ult;
        _ult->sig = nuevo;
        _ult = nuevo;
    }
}

void Lista::eliminar(Nat i) {
    Nodo* actual = _pri;
    for (int j = 0; j < i; ++j) {
        actual = actual->sig;
    }
    if (actual->sig == nullptr && actual->ant == nullptr) { //caso es el único nodo
        _pri = nullptr;
        _ult = nullptr;
        delete actual;
        return;
    }
    if (actual->sig == nullptr) { //caso es el último nodo
        _ult = actual->ant;
        actual->ant->sig = nullptr;
        delete actual;
        return;
    }
    if (actual->ant == nullptr) { //caso es el primer nodo
        _pri = actual->sig;
        actual->sig->ant = nullptr;
        delete actual;
        return;
    }
    actual->ant->sig = actual->sig; //caso no es el primero ni el último
    actual->sig->ant = actual->ant;
    delete actual;

}

int Lista::longitud() const {
    Nodo* actual = _pri;
    int contador = 0;
    while (actual != nullptr) {
        contador++;
        actual = actual->sig;
    }
    return contador;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* actual = _pri;
    for (int j = 0; j < i; ++j) {
        actual = actual->sig;
    }
    return actual->valor;
}

int& Lista::iesimo(Nat i) { //(es igual a la anterior...)
    Nodo* actual = _pri;
    for (int j = 0; j < i; ++j) {
        actual = actual->sig;
    }
    return actual->valor;
}

void Lista::mostrar(ostream& o) {
    o << "[";
    for (int i = 0; i < this->longitud(); ++i) {
        o << this->iesimo(i);
        if (i < this->longitud() - 1) {
            o << ", ";
        }
    }
    o << "]";
}
